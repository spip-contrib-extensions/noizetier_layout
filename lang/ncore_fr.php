<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'type_noisette_conteneur_titre' => 'Bloc de colonnage',
	'type_noisette_conteneur_description' => 'Bloc permettant d’afficher les noisettes enfantes sous forme de colonnes.',
);
