<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noizetier_layout_nom' => 'NoiZetier : agencements',
	'noizetier_layout_slogan' => 'Gestion de l\'agencement des noisettes.',
	'noizetier_layout_description' => 'Ce plugin ajoute la prise en charge des grilles CSS pour le Noizetier, ce qui permet de gérer l\'agencement des noisettes directement depuis l\'espace privé : nombre de colonnes, alignements, etc. Il ne fait rien tant qu\'une grille CSS n\'est pas déclarée par un autre plugin.',
);
